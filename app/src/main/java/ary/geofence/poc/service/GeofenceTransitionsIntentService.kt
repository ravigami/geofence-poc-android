package ary.geofence.poc.service

import android.app.IntentService
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.TaskStackBuilder
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Color
import android.support.v4.app.NotificationCompat
import android.text.TextUtils
import android.util.Log
import ary.geofence.poc.view.GeofenceMapsActivity
import ary.geofence.poc.R
import ary.geofence.poc.util.GeofenceUtil
import ary.geofence.poc.util.NotificationUtil
import ary.geofence.poc.util.NotificationUtil.DEFAULT_CHANNEL_ID
import com.google.android.gms.location.Geofence
import com.google.android.gms.location.GeofencingEvent
import java.util.*

class GeofenceTransitionsIntentService : IntentService(TAG) {

    companion object {
        val TAG = "GeofenceTransitionsIS"
    }

    init {
        Log.d(TAG, "GeofenceTransitionsIntentService()")
    }

    override fun onHandleIntent(intent: Intent?) {
        Log.d(GeofenceUpdatesBroadcastReceiver.TAG, "Geofence event received in GeofenceTransitionsIntentService")
        if (intent != null) {
            GeofenceUtil.parseAndHandleGeofenceTrigger(intent, context = this)
        }
    }
}
