/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ary.geofence.poc.service

import android.annotation.TargetApi
import android.app.NotificationChannel.DEFAULT_CHANNEL_ID
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.TaskStackBuilder
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Color
import android.os.Build
import android.support.v4.app.NotificationCompat
import android.text.TextUtils
import android.util.Log
import android.widget.Toast
import ary.geofence.poc.R
import ary.geofence.poc.util.GeofenceUtil
import ary.geofence.poc.util.NotificationUtil
import ary.geofence.poc.util.NotificationUtil.GEOFENCE_CHANNEL_ID
import ary.geofence.poc.view.GeofenceMapsActivity
import com.google.android.gms.location.Geofence
import com.google.android.gms.location.GeofencingEvent
import java.util.*

/**
 * Receiver for handling location updates.
 *
 * For apps targeting API level O
 * {@link android.app.PendingIntent#getBroadcast(Context, int, Intent, int)} should be used when
 * requesting location updates. Due to limits on background services,
 * {@link android.app.PendingIntent#getService(Context, int, Intent, int)} should not be used.
 *
 *  Note: Apps running on "O" devices (regardless of targetSdkVersion) may receive updates
 *  less frequently than the interval specified in the
 *  {@link com.google.android.gms.location.LocationRequest} when the app is no longer in the
 *  foreground.
 */
class GeofenceUpdatesBroadcastReceiver : BroadcastReceiver() {

    companion object {
        val TAG = "GeofenceUpdatesBR"
    }

    init {
        Log.d(GeofenceUpdatesBroadcastReceiver.TAG, "GeofenceUpdatesBroadcastReceiver()")
    }

    private var context: Context? = null

    @TargetApi(Build.VERSION_CODES.O)
    override fun onReceive(context: Context, intent: Intent) {
        Log.d(GeofenceUpdatesBroadcastReceiver.TAG, "Geofence event received in GeofenceUpdatesBroadcastReceiver")

        this.context = context
        if (intent != null) {
            GeofenceUtil.parseAndHandleGeofenceTrigger(intent, context)
        }
    }
}
