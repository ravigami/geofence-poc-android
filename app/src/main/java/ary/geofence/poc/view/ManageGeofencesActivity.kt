package ary.geofence.poc.view

import android.os.Bundle
import android.os.Handler
import android.support.v7.widget.LinearLayoutManager
import android.view.MenuItem
import android.view.View
import ary.geofence.poc.R
import ary.geofence.poc.base.BaseActivity
import ary.geofence.poc.database.DbWorkerThread
import ary.geofence.poc.database.Geofence
import ary.geofence.poc.database.GeofenceDatabase
import ary.geofence.poc.view.adapter.GeofenceAdapter
import com.google.android.gms.location.LocationServices
import kotlinx.android.synthetic.main.activity_manage_geofences.*
import org.jetbrains.anko.toast

class ManageGeofencesActivity : BaseActivity() {

    private lateinit var geofenceAdapter: GeofenceAdapter
    private var geoFencesList: ArrayList<Geofence> = ArrayList()
    private var geofenceDatabase: GeofenceDatabase? = null
    private lateinit var dbWorkerThread: DbWorkerThread
    private val uiHandler = Handler()

    companion object {
        val TAG = "ManageGeofencesActivity"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_manage_geofences)

        supportActionBar?.setTitle(R.string.title_activity_manage_geofence)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        initServicesAndDb()
        setUiElements()
    }

    private fun initServicesAndDb() {
        dbWorkerThread = DbWorkerThread("dbWorkerThread")
        dbWorkerThread.start()
        geofenceDatabase = GeofenceDatabase.getInstance(this)
    }

    private fun setUiElements() {
        rvGeofences.layoutManager = LinearLayoutManager(this)
        geofenceAdapter = GeofenceAdapter(geoFencesList, this) { geofenceItem: Geofence -> geoFenceItemDeleted(geofenceItem) }
        rvGeofences.adapter = geofenceAdapter

        deleteAllGeofencesBtn.setOnClickListener {
            var geofencesRequestIds: ArrayList<String> = ArrayList();
            for (geofence: Geofence in geoFencesList) {
                geofencesRequestIds.add(geofence.name)
            }

            val geofencingClient = LocationServices.getGeofencingClient(this)
            geofencingClient.removeGeofences(geofencesRequestIds)?.run {
                addOnSuccessListener {
                    deleteAllGeofences()
                    geoFencesList.clear()
                    plotUI()
                    toast(getString(R.string.all_geofence_items_deleted))
                }
                addOnFailureListener {
                    toast(getString(R.string.error_delete_geofence))
                }
            }
        }

        retrieveGeofencesFromDb()
    }

    private fun geoFenceItemDeleted(geofenceItem: Geofence) {
        val geofencingClient = LocationServices.getGeofencingClient(this)
        geofencingClient.removeGeofences(listOf(geofenceItem.name))?.run {
            addOnSuccessListener {
                deleteGeofenceFromDb(geofenceItem)
                geoFencesList.remove(geofenceItem)
                plotUI()
                toast(getString(R.string.geofence_item_deleted))
            }
            addOnFailureListener {
                toast(getString(R.string.error_delete_geofence))
            }
        }
    }

    private fun plotUI() {
        if (geoFencesList == null || geoFencesList?.size == 0) {
            rvGeofences.visibility = View.GONE
            infoTextView.visibility = View.VISIBLE
            deleteAllGeofencesBtn.isEnabled = false
        } else {
            rvGeofences.visibility = View.VISIBLE
            infoTextView.visibility = View.GONE
            deleteAllGeofencesBtn.isEnabled = true
            geofenceAdapter.setGeofenceItems(geoFencesList)
        }
    }

    private fun retrieveGeofencesFromDb() {
        val task = Runnable {
            geofenceDatabase?.geofenceDao()?.getAll()?.let { geoFencesList.addAll(it) }
            uiHandler.post {
                plotUI()
            }
        }
        dbWorkerThread.postTask(task)
    }

    private fun deleteGeofenceFromDb(geofence: ary.geofence.poc.database.Geofence) {
        val task = Runnable { geofenceDatabase?.geofenceDao()?.deleteGeofence(geofence) }
        dbWorkerThread.postTask(task)
    }

    private fun deleteAllGeofences() {
        val task = Runnable { geofenceDatabase?.geofenceDao()?.deleteAll() }
        dbWorkerThread.postTask(task)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        onBackPressed()
        return super.onOptionsItemSelected(item)
    }

    override fun onDestroy() {
        GeofenceDatabase.destroyInstance()
        dbWorkerThread.quit()

        super.onDestroy()
    }
}
