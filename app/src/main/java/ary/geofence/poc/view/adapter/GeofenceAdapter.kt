package ary.geofence.poc.view.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import ary.geofence.poc.R
import ary.geofence.poc.database.Geofence
import kotlinx.android.synthetic.main.item_geofence.view.*

class GeofenceAdapter(var geofencesItems: ArrayList<Geofence>, val context: Context, val clickListener: (Geofence) -> Unit) : RecyclerView.Adapter<GeofenceViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GeofenceViewHolder {
        return GeofenceViewHolder(LayoutInflater.from(context).inflate(R.layout.item_geofence, parent, false))
    }

    fun setGeofenceItems(items: ArrayList<Geofence>) {
        geofencesItems = items
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: GeofenceViewHolder, position: Int) {
        holder.bind(geofencesItems[position], context, clickListener)
    }

    override fun getItemCount(): Int {
        return geofencesItems.size
    }
}

class GeofenceViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    fun bind(geofence: Geofence, context: Context, clickListener: (Geofence) -> Unit) {
        itemView.geofence.text = geofence?.name
        itemView.latLng.text = String.format(context.resources.getString(R.string.lat_lng_placeholder),
                geofence?.lat.toString(),
                geofence?.lon.toString())

        itemView.radius?.text = String.format(context.resources.getString(R.string.radius_placeholder),
                geofence?.radius.toString())

        itemView.deleteGeofenceBtn?.setOnClickListener { clickListener(geofence) }
    }
}
