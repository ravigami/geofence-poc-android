package ary.geofence.poc.view

import android.Manifest
import android.annotation.SuppressLint
import android.app.PendingIntent
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.MenuItem
import ary.geofence.poc.R
import ary.geofence.poc.base.BaseActivity
import ary.geofence.poc.database.DbWorkerThread
import ary.geofence.poc.database.GeofenceDatabase
import ary.geofence.poc.service.GeofenceTransitionsIntentService
import ary.geofence.poc.service.GeofenceUpdatesBroadcastReceiver
import com.google.android.gms.location.Geofence
import com.google.android.gms.location.Geofence.NEVER_EXPIRE
import com.google.android.gms.location.GeofencingClient
import com.google.android.gms.location.GeofencingRequest
import com.google.android.gms.location.GeofencingRequest.INITIAL_TRIGGER_ENTER
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.model.LatLng
import kotlinx.android.synthetic.main.activity_add_geofence.*
import org.jetbrains.anko.toast
import java.util.*

class AddGeofenceActivity : BaseActivity() {

    private lateinit var geofencingClient: GeofencingClient
    private lateinit var geofenceList: ArrayList<Geofence>

    private var geofenceDatabase: GeofenceDatabase? = null
    private lateinit var dbWorkerThread: DbWorkerThread
    private val uiHandler = Handler()

    companion object {
        val TAG = "AddGeofenceActivity"
    }

    private val geofencePendingIntent: PendingIntent by lazy {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.N_MR1) {
            val intent = Intent(this, GeofenceUpdatesBroadcastReceiver::class.java)
            PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
        } else {
            val intent = Intent(this, GeofenceTransitionsIntentService::class.java)
            PendingIntent.getService(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_geofence)

        supportActionBar?.setTitle(R.string.title_activity_add_geofence)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        initServicesAndDb()
        setUiElements()
    }

    private fun initServicesAndDb() {
        dbWorkerThread = DbWorkerThread("dbWorkerThread")
        dbWorkerThread.start()
        geofenceDatabase = GeofenceDatabase.getInstance(this)

        geofencingClient = LocationServices.getGeofencingClient(this)
    }

    private fun setUiElements() {
        val latLng: LatLng = intent.extras.getParcelable(BUNDLE_GEOFENCE_LOCATION)
        latitudeEditText.setText(latLng.latitude.toString())
        longitudeEditText.setText(latLng.longitude.toString())

        saveGeofenceBtn.setOnClickListener {
            if (isLocationEnabled) {
                if (!geofenceNameEditText.text.isNullOrBlank()
                        && !latitudeEditText.text.isNullOrBlank()
                        && !longitudeEditText.text.isNullOrBlank()
                        && !radiusEditText.text.isNullOrBlank()) {
                    if (isPermissionGranted(Manifest.permission.ACCESS_FINE_LOCATION)) {
                        prepareGeofenceList()
                        addGeofencesWithLatAndLng()
                    } else {
                        askPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                    }
                } else {
                    toast(getString(R.string.enter_geofence_parameters))
                }
            } else {
                toast(getString(R.string.turn_on_location_services))
            }
        }
    }

    private fun prepareGeofenceList() {
        val lat = latitudeEditText.text.toString().toDouble()
        val lng = longitudeEditText.text.toString().toDouble()
        val radius = radiusEditText.text.toString().toFloat()

        Log.d(TAG, "Lat = $lat Lng = $lng radius = $radius")

        geofenceList = arrayListOf<Geofence>()
        geofenceList.add(Geofence.Builder()
                .setRequestId(geofenceNameEditText.text.toString())
                .setCircularRegion(
                        lat,
                        lng,
                        radius)
                .setExpirationDuration(NEVER_EXPIRE)
                .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER or Geofence.GEOFENCE_TRANSITION_EXIT)
                .build())
    }

    private fun getGeofencingRequest(): GeofencingRequest {
        return GeofencingRequest.Builder().apply {
            setInitialTrigger(INITIAL_TRIGGER_ENTER)
            addGeofences(geofenceList)
        }.build()
    }

    @SuppressLint("MissingPermission")
    private fun addGeofencesWithLatAndLng() {
        geofencingClient.addGeofences(getGeofencingRequest(), geofencePendingIntent)?.run {
            addOnSuccessListener {
                toast(getString(R.string.geofence_success))
                saveGeofenceData()
                geofenceList.clear()

                finish()
            }
            addOnFailureListener {
                toast(getString(R.string.geofence_error) + " " + it.localizedMessage)
            }
        }
    }

    private fun saveGeofenceData() {
        var geofenceModel = ary.geofence.poc.database.Geofence()

        val lat = latitudeEditText.text.toString().toDouble()
        val lng = longitudeEditText.text.toString().toDouble()
        val radius = radiusEditText.text.toString().toFloat()

        geofenceModel.name = geofenceNameEditText.text.toString()
        geofenceModel.lat = lat
        geofenceModel.lon = lng
        geofenceModel.radius = radius

        insertGeofenceInDb(geofenceModel)
    }

    private fun insertGeofenceInDb(geofence: ary.geofence.poc.database.Geofence) {
        val task = Runnable { geofenceDatabase?.geofenceDao()?.insert(geofence) }
        dbWorkerThread.postTask(task)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        onBackPressed()
        return super.onOptionsItemSelected(item)
    }

    override fun onRequestPermissionsResult(
            requestCode: Int,
            thePermissions: Array<String>,
            theGrantResults: IntArray) {

        // It's not our expect permission
        if (requestCode != REQ_PERMISSION) return

        if (isPermissionGranted(Manifest.permission.ACCESS_FINE_LOCATION)) {
            prepareGeofenceList()
            addGeofencesWithLatAndLng()
        } else {
            toast(getString(R.string.permission_not_granted))
            finish()
        }
    }

    override fun onDestroy() {
        GeofenceDatabase.destroyInstance()
        dbWorkerThread.quit()

        super.onDestroy()
    }
}
