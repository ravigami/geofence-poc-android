package ary.geofence.poc.view

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.location.Location
import android.os.Bundle
import android.util.Log
import ary.geofence.poc.R
import ary.geofence.poc.base.BaseActivity
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.activity_geofence_maps.*
import org.jetbrains.anko.toast
import com.crashlytics.android.Crashlytics
import io.fabric.sdk.android.Fabric




class GeofenceMapsActivity : BaseActivity(), OnMapReadyCallback {
    private lateinit var map: GoogleMap
    private val ZOOM_LEVEL = 17.0f

    private var lastLocationLatLng = LatLng(-34.0, 151.0)
    private var lastLocation: Location? = null
    private var locationRequest: LocationRequest? = null
    private var fusedLocationClient: FusedLocationProviderClient? = null
    private lateinit var locationCallback: LocationCallback

    private val UPDATE_INTERVAL = (2 * 1000).toLong()  /* 10 secs */
    private val FASTEST_INTERVAL: Long = 2000 /* 2 sec */

    companion object {
        val TAG = "GeofenceMapsActivity"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Fabric.with(this, Crashlytics())
        setContentView(R.layout.activity_geofence_maps)

        supportActionBar?.setTitle(R.string.title_activity_geofence_maps)
        val mapFragment: SupportMapFragment? =
                supportFragmentManager.findFragmentById(R.id.map) as? SupportMapFragment

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                locationResult ?: return
                for (location in locationResult.locations) {
                    lastLocation = location
                    map.clear()
                    addLocationMarker(LatLng(lastLocation!!.latitude, lastLocation!!.longitude), getString(R.string.current_location))
                    map.animateCamera(CameraUpdateFactory.zoomTo(ZOOM_LEVEL))
                    break
                }
            }
        }

        mapFragment?.getMapAsync(this)
        setUiElements()
    }

    override fun onResume() {
        super.onResume()
        startLocationUpdates()
    }

    override fun onPause() {
        super.onPause()
        stopLocationUpdates()
    }

    private fun stopLocationUpdates() {
        fusedLocationClient?.removeLocationUpdates(locationCallback)
    }

    private fun setUiElements() {
        setLocationBtn.setOnClickListener {
            val intent = Intent(this@GeofenceMapsActivity, AddGeofenceActivity::class.java)
            intent.putExtra(BUNDLE_GEOFENCE_LOCATION, lastLocationLatLng)
            startActivity(intent)
        }

        manageGeofenceBtn.setOnClickListener {
            val intent = Intent(this@GeofenceMapsActivity, ManageGeofencesActivity::class.java)
            startActivity(intent)
        }
    }

    @SuppressLint("MissingPermission")
    private fun getLastLocation() {
        fusedLocationClient!!.lastLocation
                .addOnCompleteListener(this) { task ->
                    if (task.isSuccessful && task.result != null) {
                        lastLocation = task.result
                        map.clear()
                        addLocationMarker(LatLng(lastLocation!!.latitude, lastLocation!!.longitude), getString(R.string.current_location))
                        map.animateCamera(CameraUpdateFactory.zoomTo(ZOOM_LEVEL))
                    } else {
                        Log.w(TAG, "getLastLocation:exception", task.exception)
                        toast(getString(R.string.no_location_detected))
                    }
                }
    }

    private fun addLocationMarker(latLng: LatLng, title: String?) {
        lastLocationLatLng = latLng
        map.addMarker(MarkerOptions().position(latLng).title(title))
        map.moveCamera(CameraUpdateFactory.newLatLng(latLng))
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        addLocationMarker(lastLocationLatLng, when (lastLocation != null) {
            true -> getString(R.string.current_location)
            false -> getString(R.string.sydney_location)
        })

        map.animateCamera(CameraUpdateFactory.zoomTo(ZOOM_LEVEL))

        map.setOnMapClickListener {
            toast(it?.latitude.toString() + ", " + it?.longitude.toString())
            map.clear()
            addLocationMarker(it, getString(R.string.current_location))
        }

        if (isPermissionGranted(Manifest.permission.ACCESS_FINE_LOCATION)) {
            getLastLocation()
        } else {
            askPermission(Manifest.permission.ACCESS_FINE_LOCATION)
        }
    }

    @SuppressLint("MissingPermission")
    private fun startLocationUpdates() {
        locationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(UPDATE_INTERVAL)
                .setFastestInterval(FASTEST_INTERVAL);
        // Request location updates
        if (isPermissionGranted(Manifest.permission.ACCESS_FINE_LOCATION)) {
            return
        }

        fusedLocationClient?.requestLocationUpdates(locationRequest,
                locationCallback,
                null /* Looper */)
    }

    override fun onRequestPermissionsResult(
            requestCode: Int,
            thePermissions: Array<String>,
            theGrantResults: IntArray) {

        // It's not our expect permission
        if (requestCode != REQ_PERMISSION) return

        if (isPermissionGranted(Manifest.permission.ACCESS_FINE_LOCATION)) {
            getLastLocation()
        } else {
            toast(getString(R.string.permission_not_granted))
            finish()
        }
    }
}
