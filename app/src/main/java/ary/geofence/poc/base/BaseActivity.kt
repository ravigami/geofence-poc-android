package ary.geofence.poc.base

import android.content.Context
import android.content.pm.PackageManager
import android.location.LocationManager
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity

open class BaseActivity : AppCompatActivity() {

    val BUNDLE_GEOFENCE_LOCATION: String? = "GEOFENCE_LOCATION";
    private var locationManager: LocationManager? = null

    companion object {
        val REQ_PERMISSION: Int = 100
    }

    val isLocationEnabled: Boolean
        get() {
            locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
            return locationManager!!.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager!!.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
        }

    fun isPermissionGranted(permission: String): Boolean =
            ContextCompat.checkSelfPermission(
                    this,
                    permission
            ) == PackageManager.PERMISSION_GRANTED

    fun askPermission(permission: String) {
        ActivityCompat.requestPermissions(this,
                arrayOf(permission), REQ_PERMISSION)
    }
}