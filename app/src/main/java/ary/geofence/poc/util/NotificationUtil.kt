package ary.geofence.poc.util

import android.annotation.TargetApi
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.TaskStackBuilder
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Color
import android.os.Build
import android.support.annotation.RequiresApi
import android.support.v4.app.NotificationCompat
import ary.geofence.poc.R
import ary.geofence.poc.view.GeofenceMapsActivity
import java.util.*

object NotificationUtil {

    private val MIN = 0
    private val MAX = 10000
    const val DEFAULT_CHANNEL_ID = "DEFAULT_CHANNEL_ID"
    const val GEOFENCE_CHANNEL_ID = "GEOFENCE_CHANNEL_ID"

    @TargetApi(Build.VERSION_CODES.O)
    fun createGeofenceChannel(context: Context) {
        val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val geofenceChannel = NotificationChannel(
                GEOFENCE_CHANNEL_ID,
                context.resources.getString(R.string.notification_channel_genfence),
                NotificationManager.IMPORTANCE_HIGH)

        geofenceChannel.lightColor = Color.GREEN
        geofenceChannel.vibrationPattern = longArrayOf(100, 200, 300, 400, 500, 400, 500, 200, 500)

        notificationManager.createNotificationChannel(geofenceChannel)
    }

    fun showNotification(context: Context, title: String, description: String, channelId: String) {
        val notificationIntent = Intent(context, GeofenceMapsActivity::class.java)

        val stackBuilder = TaskStackBuilder.create(context)
        stackBuilder.addParentStack(GeofenceMapsActivity::class.java)
        stackBuilder.addNextIntent(notificationIntent)

        val notificationPendingIntent = stackBuilder.getPendingIntent(0,
                PendingIntent.FLAG_UPDATE_CURRENT)

        val builder = NotificationCompat.Builder(context, channelId)

        builder.setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(BitmapFactory.decodeResource(context.resources, R.mipmap.ic_launcher))
                .setColor(Color.RED)
                .setContentTitle(title)
                .setContentText(description)
                .setContentIntent(notificationPendingIntent)

        builder.setAutoCancel(true)
        val mNotificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        mNotificationManager.notify(getRandomNotificationId(), builder.build())
    }

    private fun getRandomNotificationId(): Int {
        return Random().nextInt(MAX - MIN + 1) + MIN
    }
}