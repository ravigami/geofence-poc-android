package ary.geofence.poc.util

import android.content.Context
import android.content.Intent
import android.os.Build
import android.text.TextUtils
import android.util.Log
import ary.geofence.poc.R
import com.google.android.gms.location.Geofence
import com.google.android.gms.location.GeofencingEvent

object GeofenceUtil {

    val TAG = "GeofenceUtil"

    fun parseAndHandleGeofenceTrigger(intent: Intent, context: Context) {

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.N_MR1) {
            NotificationUtil.createGeofenceChannel(context.applicationContext)
        }

        val geofencingEvent = GeofencingEvent.fromIntent(intent)
        if (geofencingEvent.hasError()) {
            Log.e(GeofenceUtil.TAG, "Geofencing event error: " + geofencingEvent.errorCode)
            return
        }

        val geofenceTransition = geofencingEvent.geofenceTransition
        if (geofenceTransition == Geofence.GEOFENCE_TRANSITION_ENTER || geofenceTransition == Geofence.GEOFENCE_TRANSITION_EXIT) {
            val triggeringGeofences = geofencingEvent.triggeringGeofences
            val geofenceTransitionDetails = getGeofenceTransitionDetails(context, geofenceTransition, triggeringGeofences)
            Log.i(GeofenceUtil.TAG, geofenceTransitionDetails)

            NotificationUtil.showNotification(context = context.applicationContext,
                    title = geofenceTransitionDetails,
                    description = context.resources.getString(R.string.geofence_transition_notification_text),
                    channelId = when (Build.VERSION.SDK_INT > Build.VERSION_CODES.N_MR1) {
                        true -> NotificationUtil.GEOFENCE_CHANNEL_ID
                        false -> NotificationUtil.DEFAULT_CHANNEL_ID
                    })
        } else {
            Log.e(GeofenceUtil.TAG, context.resources.getString(R.string.geofence_transition_invalid_type, geofenceTransition))
        }
    }

    private fun getGeofenceTransitionDetails(context: Context, geofenceTransition: Int, triggeringGeofences: List<Geofence>): String {
        val geofenceTransitionString = getTransitionString(context, geofenceTransition)
        val triggeringGeofencesIdsList = triggeringGeofences.map { geofence -> geofence.requestId }
        return when (geofenceTransition) {
            Geofence.GEOFENCE_TRANSITION_ENTER -> String.format(context.resources.getString(R.string.notification_enter_place_holder),
                    geofenceTransitionString,
                    TextUtils.join(", ", triggeringGeofencesIdsList))

            Geofence.GEOFENCE_TRANSITION_EXIT -> String.format(context!!.resources.getString(R.string.notification_exit_place_holder),
                    geofenceTransitionString,
                    TextUtils.join(", ", triggeringGeofencesIdsList))

            else -> context.resources.getString(R.string.unknown_geofence_transition)
        }
    }

    private fun getTransitionString(context: Context, transitionType: Int): String {
        return when (transitionType) {
            Geofence.GEOFENCE_TRANSITION_ENTER -> context!!.resources.getString(R.string.geofence_transition_entered)
            Geofence.GEOFENCE_TRANSITION_EXIT -> context.resources.getString(R.string.geofence_transition_exited)
            else -> context!!.resources.getString(R.string.unknown_geofence_transition)
        }
    }

}