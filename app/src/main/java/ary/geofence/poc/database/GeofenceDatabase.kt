package ary.geofence.poc.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context

@Database(entities = arrayOf(Geofence::class), version = 1)
abstract class GeofenceDatabase : RoomDatabase() {

    abstract fun geofenceDao(): GeofenceDao

    companion object {
        private var INSTANCE: GeofenceDatabase? = null

        fun getInstance(context: Context): GeofenceDatabase? {
            if (INSTANCE == null) {
                synchronized(GeofenceDatabase::class) {
                    INSTANCE = Room.databaseBuilder(context.applicationContext,
                            GeofenceDatabase::class.java, "geofence.db")
                            .build()
                }
            }
            return INSTANCE
        }

        fun destroyInstance() {
            //INSTANCE = null
        }
    }
}
