package ary.geofence.poc.database

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy.REPLACE
import android.arch.persistence.room.Query
import android.arch.persistence.room.Delete


@Dao
interface GeofenceDao {

    @Query("SELECT * from Geofence")
    fun getAll(): List<Geofence>

    @Insert(onConflict = REPLACE)
    fun insert(geofence: Geofence)

    @Query("DELETE from Geofence")
    fun deleteAll()

    @Delete
    fun deleteGeofence(geofence: Geofence)
}