package ary.geofence.poc.database

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "Geofence")
data class Geofence(@PrimaryKey(autoGenerate = true) var id: Long?,
                    @ColumnInfo(name = "name") var name: String,
                    @ColumnInfo(name = "lat") var lat: Double,
                    @ColumnInfo(name = "lon") var lon: Double,
                    @ColumnInfo(name = "radius") var radius: Float
) {
    constructor() : this(null, "", 0.0, 0.0, 0.0f)
}